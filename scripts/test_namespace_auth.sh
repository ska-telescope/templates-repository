#!/bin/bash
echo "Starting Test..."
echo "Please export CAR credentials if you want third test to pass"
# set -eo pipefail
# Define branch name
export CI_PROJECT_NAME=project-foo
export CI_COMMIT_REF_SLUG=branch-bar

# Test wrong namespace
echo "########################## Test 1: Start ###########################"
NAMESPACE=ci-test-${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}
SERVICE_ACCOUNT=service-account-baz-wrong
echo "NAMESPACE: ${NAMESPACE}"
kubectl create ns ${NAMESPACE} || true
sh ./namespace_auth.sh ${SERVICE_ACCOUNT} ${NAMESPACE}
if [[ $? == 0 ]]; then
  echo -e "\033[0;31mTest failed!\033[0m"
  exit 1
else
  echo -e "\033[0;32mWrong Namespace test successfull!:\033[0m"
fi
kubectl delete --ignore-not-found ns ${NAMESPACE}
echo "########################## Test 1: Finish ###########################"
echo "########################## Test 2: Start ###########################"
# Test no namespaces

NAMESPACE=ci-${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}-no-ns
echo "NAMESPACE: ${NAMESPACE}"
sh ./namespace_auth.sh ${SERVICE_ACCOUNT} ${NAMESPACE}
if [[ $? == 0 ]]; then
  echo -e "\033[0;31mTest failed!\033[0m"
  exit 1
else
  echo -e "\033[0;32mNo Namespace test successfull!:\033[0m"
fi

echo "########################## Test 2: Finish ###########################"
echo "########################## Test 3: Start ###########################"
# Start the SSH tunnel for the cache instance
NEXUS_CACHE_UPSTREAM=192.168.99.204
touch /tmp/ssh-temp-socket
ssh -f -N -M -S /tmp/ssh-temp-socket -L 8081:localhost:8081 ubuntu@${NEXUS_CACHE_UPSTREAM}
# Override cache for localhost
export NEXUS_CACHE=localhost:8081
# Create test namespaces
NAMESPACE=ci-${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}-mid
SERVICE_ACCOUNT=service-account-baz
kubectl create ns ${NAMESPACE} || true
kubectl create ns ${NAMESPACE}-sdp || true
echo "NAMESPACE: ${NAMESPACE}"
# Call the script and download the kubeconfig file
sh ./namespace_auth.sh ${SERVICE_ACCOUNT} ${NAMESPACE} ${NAMESPACE}-sdp | grep curl | sed -ne "s/^.*\(curl.*\)$/\1/p" | bash
# Check old namespaces
kubectl get ns ci-${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}
# Check if the namespace and other resources are created
kubectl -n ${NAMESPACE} get sa ${SERVICE_ACCOUNT}
kubectl -n ${NAMESPACE} get rolebinding ${SERVICE_ACCOUNT}-ns-admin
kubectl -n ${NAMESPACE} get role ${SERVICE_ACCOUNT}-ns-admin
# Check some commands with the generated kubeconfig (Download the kubeconfig and use it)
kubectl --kubeconfig=KUBECONFIG get pods
kubectl --kubeconfig=KUBECONFIG -n ${NAMESPACE}-sdp get pods
# Clean up
rm KUBECONFIG
kubectl delete ns ${NAMESPACE}
kubectl delete ns ${NAMESPACE}-sdp

ssh -S /tmp/ssh-temp-socket -O exit ${NEXUS_CACHE_UPSTREAM}

echo "########################## Test 3: Finish ###########################"
echo -e "\033[0;32mTest Successful!"